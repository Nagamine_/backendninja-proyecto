package com.desarrollo.backendninja.component;

import org.springframework.stereotype.Component;

import com.desarrollo.backendninja.entity.Contacto;
import com.desarrollo.backendninja.model.ContactoModel;

@Component
public class ContactoConverter {
	public Contacto convertModel2Entity  (ContactoModel model) {
		Contacto contacto = new Contacto();
		contacto.setId(model.getId());
		contacto.setNombre(model.getNombre());
		contacto.setApellido(model.getApellido());
		contacto.setTelefono(model.getTelefono());
		contacto.setCiudad(model.getCiudad());
		return contacto;
	}
	
	public ContactoModel convertEntity2Model (Contacto entity) {
		ContactoModel contacto = new ContactoModel();
		contacto.setId(entity.getId());
		contacto.setNombre(entity.getNombre());
		contacto.setApellido(entity.getApellido());
		contacto.setTelefono(entity.getTelefono());
		contacto.setCiudad(entity.getCiudad());
		return contacto;
	}
}
