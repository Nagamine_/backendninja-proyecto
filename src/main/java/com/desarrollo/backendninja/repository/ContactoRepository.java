package com.desarrollo.backendninja.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.desarrollo.backendninja.entity.Contacto;

@Repository
public interface ContactoRepository extends JpaRepository <Contacto,Serializable> {
	public abstract Contacto findByid(int id);
}
