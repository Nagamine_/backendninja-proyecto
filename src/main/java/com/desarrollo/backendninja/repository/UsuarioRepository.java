package com.desarrollo.backendninja.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.desarrollo.backendninja.entity.Usuario;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario,Serializable> {
	public abstract Usuario findByUsuario(String usuario);
}
