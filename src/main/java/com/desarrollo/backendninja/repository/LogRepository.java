package com.desarrollo.backendninja.repository;

import org.springframework.stereotype.Repository;

import com.desarrollo.backendninja.entity.Log;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
@Repository
public interface LogRepository extends JpaRepository<Log,Serializable>{

}
