package com.desarrollo.backendninja.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.desarrollo.backendninja.model.ContactoModel;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/rest")
public class RestController {

	@GetMapping("/checkRest")
	public ResponseEntity<String> checkRest(){
		return new ResponseEntity("OK!",HttpStatus.OK);
	}
	@GetMapping("/contacto")
	public ResponseEntity<ContactoModel> contacto(){
		ContactoModel c = new ContactoModel(5,"Alicia","kochi","23423423","madrid");
		return new ResponseEntity(c,HttpStatus.OK);
	}
}
