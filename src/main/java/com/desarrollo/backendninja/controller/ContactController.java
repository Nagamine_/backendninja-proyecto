package com.desarrollo.backendninja.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.desarrollo.backendninja.constants.ViewConstant;
import com.desarrollo.backendninja.model.ContactoModel;
import com.desarrollo.backendninja.service.ContactoService;

@Controller
@RequestMapping("/contact")
public class ContactController {
	@Autowired
	@Qualifier("contactoServiceImpl")
	private ContactoService contactoService;
	public static final Log LOG = LogFactory.getLog(ContactController.class); 
	@GetMapping("/cancel")
	public String cancel(){
		return "redirect:/contact/mostrarcontactos";
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')") //permitAll()
	@GetMapping("/contactform")
	public String redirectContactForm(Model model, @RequestParam(name="id",defaultValue  = "0" ,required=false) int id){
		ContactoModel c  = new ContactoModel();
		if(id != 0) {
			c= contactoService.buscarPorID(id);
		}
		model.addAttribute("contactoModel", c);
		return ViewConstant.CONTACT_FORM;
	}

	@PostMapping("/addcontacto")
	public String addContacto(@ModelAttribute(name="contactoModel") ContactoModel contacto, Model model){
		LOG.info("METHOD: addContacto() --PARAMS: " + contacto.toString());
		if ( null != contactoService.addContact(contacto)) {
			model.addAttribute("result", 1);
		}else {
			model.addAttribute("result", 0);
		}
		return "redirect:/contact/mostrarcontactos";
	}
	
	@GetMapping("/mostrarcontactos")
	public String mostarContactos (Model model) {
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("usuario",user.getUsername());
		model.addAttribute ("contactos",contactoService.ListarTodo());
		return ViewConstant.CONTACTS;
	}
	
	@GetMapping("/removerContacto")
	public String removeContact(Model model, @RequestParam(name="id",required=true) int id) {
		contactoService.removerContacto(id);
		return mostarContactos(model);
	}
}
