package com.desarrollo.backendninja.model;

public class UserCredential {
	private String nombre;
	private String password;
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public UserCredential() {
		super();
	}
	public UserCredential(String nombre, String password) {
		super();
		this.nombre = nombre;
		this.password = password;
	}
	@Override
	public String toString() {
		return "UserCredential [nombre=" + nombre + ", password=" + password + "]";
	}
	
	
}
