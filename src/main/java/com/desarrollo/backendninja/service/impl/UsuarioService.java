package com.desarrollo.backendninja.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.desarrollo.backendninja.entity.UserRole;
import com.desarrollo.backendninja.entity.Usuario;
import com.desarrollo.backendninja.repository.UsuarioRepository;

@Service
public class UsuarioService implements UserDetailsService{

	@Autowired
	@Qualifier("usuarioRepository")
	private UsuarioRepository userRepo;
	
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		System.out.println("username: " + username);
		Usuario user = userRepo.findByUsuario (username);
		return buildUser(user,buildAuthorities(user.getRol()));
	}
	
	private User buildUser (Usuario user , List<GrantedAuthority> autoridades){
		User u = new User(user.getUsuario(), user.getPassword(), user.isActivado(), true, true, true, autoridades);
		return u;
	}
	
	private List<GrantedAuthority> buildAuthorities (Set<UserRole> userRoles){
		List<GrantedAuthority> lista = new ArrayList<GrantedAuthority>();
		for(UserRole u : userRoles){
			lista.add(new SimpleGrantedAuthority( u.getRol()));
		}
		System.out.println("roles: " + lista);
		return lista;
	}
	
}
