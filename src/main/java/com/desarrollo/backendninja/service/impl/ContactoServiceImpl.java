package com.desarrollo.backendninja.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.desarrollo.backendninja.component.ContactoConverter;
import com.desarrollo.backendninja.entity.Contacto;
import com.desarrollo.backendninja.model.ContactoModel;
import com.desarrollo.backendninja.repository.ContactoRepository;
import com.desarrollo.backendninja.service.ContactoService;

@Service("contactoServiceImpl")
public class ContactoServiceImpl implements ContactoService {
	@Autowired
	@Qualifier("contactoRepository")
	private ContactoRepository contactoRepo;
	
	@Autowired
	@Qualifier("contactoConverter")
	private ContactoConverter contactoConverter;
	
	
	@Override
	public ContactoModel addContact(ContactoModel contacto) {
		Contacto c =contactoRepo.save(contactoConverter.convertModel2Entity(contacto));
		return contactoConverter.convertEntity2Model(c);
	}


	@Override
	public List<ContactoModel> ListarTodo() {
		List<ContactoModel> lista = new ArrayList<>();
		contactoRepo.findAll().forEach(contacto -> lista.add(contactoConverter.convertEntity2Model(contacto)));
		return lista;
	}


	@Override
	public ContactoModel buscarPorID(int id) {
		return contactoConverter.convertEntity2Model(contactoRepo.findByid(id));
	}
	


	@Override
	public void removerContacto(int id) {
		Contacto c = contactoConverter.convertModel2Entity(buscarPorID(id));
		if(c!=null) {
			contactoRepo.delete(c);
		}
	}
	
}
