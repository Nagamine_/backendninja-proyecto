package com.desarrollo.backendninja.service;

import java.util.List;

import com.desarrollo.backendninja.entity.Contacto;
import com.desarrollo.backendninja.model.ContactoModel;

public interface ContactoService {
	public abstract ContactoModel addContact(ContactoModel contacto);
	public abstract List<ContactoModel> ListarTodo(); 
	public abstract ContactoModel buscarPorID(int id);
	public abstract void removerContacto(int id);
}
