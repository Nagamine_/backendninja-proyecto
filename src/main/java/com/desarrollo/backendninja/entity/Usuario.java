package com.desarrollo.backendninja.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="usuario")
public class Usuario {
	@Id
	@Column(name="usuario",nullable=false,length=45)
	private String usuario;
	@Column(name="password",nullable=false,length=60)
	private String password;
	@Column(name="activado",nullable=false)
	private boolean activado;
	@OneToMany(fetch=FetchType.EAGER,mappedBy="usuario")
	private Set<UserRole> rol = new HashSet<UserRole>();
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public boolean isActivado() {
		return activado;
	}
	public void setActivado(boolean activo) {
		this.activado = activo;
	}
	public Set<UserRole> getRol() {
		return rol;
	}
	public void setRol(Set<UserRole> rol) {
		this.rol = rol;
	}
	public Usuario(String usuario, String password, boolean activo, Set<UserRole> rol) {
		this.usuario = usuario;
		this.password = password;
		this.activado = activo;
		this.rol = rol;
	}
	public Usuario() {
	}
	
}
